 # BUILD ENVIRONMENT
FROM anapsix/alpine-java:latest AS build

RUN mkdir /temp

COPY manifest.json /temp/manifest.json
COPY server.json /temp/server.json
COPY modpackdownloader.jar /temp/modpackdownloader.jar
WORKDIR /temp
RUN java -jar modpackdownloader.jar

FROM kerts93/thesociety-minecraft:6
COPY --from=build /temp /data
COPY configs /data/config
COPY extra-mods /data/mods
RUN mv /data/resourcepacks /data/config/immersiverailroading
RUN chown -R minecraft:minecraft /data